package demo.home;

import java.io.Serializable;

/**
 *
 * @author antoniodifluri
 */
public class AgencyResource implements Serializable{
    
    
    
    private Integer id;
    private String name;
    private String EIN;
    
    public AgencyResource(Integer id, String name, String eIN) {
        super();
        this.id = id;
        this.name = name;
        EIN = eIN;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the EIN
     */
    public String getEIN() {
        return EIN;
    }

    /**
     * @param EIN the EIN to set
     */
    public void setEIN(String EIN) {
        this.EIN = EIN;
    }
    
}
