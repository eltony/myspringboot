package demo.info;

import demo.dao.BeanDao;
import demo.entity.BeanEntity;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.transaction.annotation.Transactional;
 


@Service
public class CtxBeanService {
    
    private static final Logger LOGGER = Logger.getLogger(CtxBeanService.class.getName());

    @Autowired
    private ApplicationContext appContext;
    
    @Autowired
    private BeanDao beanDao;

    public List<String> getBeanDefinitionNames() {
        LOGGER.log(Level.INFO, "getting all beans");
        return Arrays.asList(appContext.getBeanDefinitionNames());
    }

    public CtxBeanInfo getCtxBeanDetail(String name) {

        LOGGER.log(Level.INFO, "getting bean detail for {0}", name);
        
        Object obj = appContext.getBean(name);

        Method[] methods = obj.getClass().getMethods();

        List<String> filteredMethods = new ArrayList<>();

        for (Method m : methods) {
            if (Modifier.isPublic(m.getModifiers())) {
                filteredMethods.add(m.getName());
            }
        }

        return new CtxBeanInfo(name, filteredMethods);

    }
    
    public List<String> getCtxBeanMethods(String name) {

        LOGGER.log(Level.INFO, "getting bean detail for {0}", name);
        
        Object obj = appContext.getBean(name);

        Method[] methods = obj.getClass().getDeclaredMethods();

        Set<String> filteredMethods = new HashSet<>();

        for (Method m : methods) {
            if (Modifier.isPublic(m.getModifiers())) {
                filteredMethods.add(m.getName());
            }
        }

        List<String> all = new ArrayList<>(filteredMethods);
        return all;

    }
    
    public List<BeanEntity> getDbBeans(){
    
        return beanDao.selectAll();
        
    }
    
    @Transactional
    public void initDbBeans(){
        
        BeanEntity be = new BeanEntity();
        be.setName("name1");
        beanDao.save(be);
        
        be = new BeanEntity();
        be.setName("name2");
        beanDao.save(be);
        
        be = new BeanEntity();
        be.setName("name3");
        beanDao.save(be);
        
        be = new BeanEntity();
        be.setName("name4");
        beanDao.save(be);
    
    } 
}
