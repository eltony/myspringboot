package demo.info;

import demo.entity.BeanEntity;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BeanInfoController {

    @Autowired
    private CtxBeanService ctxBeanService;
    
    @RequestMapping("/ctxBeans")
    @ResponseBody
    public List<String> getCtxBeans() {
        return ctxBeanService.getBeanDefinitionNames();
    }
    
    @RequestMapping("/ctxBeanDetail")
    @ResponseBody
    public CtxBeanInfo getCtxBeanDetail(String name) {
        
        return ctxBeanService.getCtxBeanDetail(name);
        
    }
    
    @RequestMapping("/ctxBeanMethods")
    @ResponseBody
    public List<String> getCtxBeanMethods(String name) {
        
        return ctxBeanService.getCtxBeanMethods(name);
        
    }
    
    @RequestMapping("/dbBeans")
    @ResponseBody
    public List<BeanEntity> getDbBeans() {
        return ctxBeanService.getDbBeans();
    }
    
    @RequestMapping("/initDbBeans")
    public void initDbBeans(){
        ctxBeanService.initDbBeans();
    }

}
