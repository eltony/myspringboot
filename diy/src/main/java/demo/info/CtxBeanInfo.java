package demo.info;

import java.io.Serializable;
import java.util.Collection;

public class CtxBeanInfo implements Serializable{
    
    private String name;
    
    private Collection<String> methods; 

    public CtxBeanInfo(String name, Collection<String> methods) {
        this.name = name;
        this.methods = methods;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the methods
     */
    public Collection<String> getMethods() {
        return methods;
    }

    /**
     * @param methods the methods to set
     */
    public void setMethods(Collection<String> methods) {
        this.methods = methods;
    }
    
    
    
}
