package demo.cafe;

import java.util.List;

public class Waiter {
	
	public Delivery prepareDelivery(List<Drink> drinks) {
		return new Delivery(drinks);
	}

	public int correlateByOrderNumber(Drink drink) {
		return drink.getOrderNumber();
	}


}
