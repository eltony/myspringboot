package demo;

import org.springframework.stereotype.Service;

@Service
public class TestService {

	public String getName() {
		return TestService.class.getName();
	}
        
        public String getDate() {
		return "date " + System.nanoTime();
	}
}