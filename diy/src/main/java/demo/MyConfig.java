package demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:/myconfig.xml", "classpath:/myintegration.xml"})
public class MyConfig { }
