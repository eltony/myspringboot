package demo.dao;

import demo.entity.BeanEntity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("beanDao")
@Transactional(propagation = Propagation.REQUIRED)
public class BeanDao {

    private static final String SELECT_QUERY = "select be from BeanEntity be";

    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void save(BeanEntity bean) {
        entityManager.persist(bean);
    }

    public List<BeanEntity> selectAll() {
        Query query = entityManager.createQuery(SELECT_QUERY);
        List<BeanEntity> beans = (List<BeanEntity>) query.getResultList();
        return beans;
    }
}
