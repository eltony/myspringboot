'use strict';

var mymodule = angular.module('myApp.beans', ['ngRoute','ui.bootstrap']);

mymodule.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/beans', {
    templateUrl: 'beans/beans.html',
    controller: 'AccordionDemoCtrl'
  });
}]);

mymodule.controller('AccordionDemoCtrl', function ($scope, $http, $log) {

    $scope.oneAtATime = true;
    $scope.methods = [];

    $http.get(jsonCallUrl+'/ctxBeans').
            success(function (data, status, headers, config) {
                $scope.mybeans = data;
            }).
            error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

    $scope.beanDetail = function (bean) {

        $http.get(jsonCallUrl+'/ctxBeanMethods?name=' + bean, {cache:true}).
                success(function (data, status, headers, config) {
                    $log.debug(data);
                    $scope.methods[bean] = data;
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $scope.methods[bean] = ['errors...'];
                });

    };

    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
});